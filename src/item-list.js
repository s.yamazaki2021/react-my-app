import React, {useState} from "react";
import {Checkbox, List, ListItem, ListItemText} from "@material-ui/core";

//一覧するデータを状態(state)として設定
const initialItems = [
    {id: 1, contents: 'やること1', checked: false},
    {id: 2, contents: 'やること2', checked: true},
    {id: 3, contents: 'やること3', checked: false},
    {id: 4, contents: 'やること4', checked: false},
];

const ItemList = () => {
    const [items, setItems] = useState(initialItems);
    const toggleChecked = (item) => {
        // itemはリスト内の1行分のデータ
        // 戻り値はチェックボックスをON/OFFした際に動作する関数
        return () => {
            // 一覧データから対象データを探索
            const targetIndex = items.indexOf(item);
            // 対象データのチェック状態を変更（true⇔false）
            items[targetIndex].checked = !items[targetIndex].checked;
            // 状態(state)に変更後の一覧データを変更用の関数を使って反映
            setItems([...items]);
        };
    }
    return (
        <List>
            {items.map(item => (
                // ListItemはList内の一行分の表示を行うコンポーネント
                <ListItem key={item.id}>
                    {/* Checkboxは、チェックボックス用のコンポーネント */}
                    <Checkbox
                        checked={item.checked}
                        onChange={toggleChecked(item)}
                    />
                    {/* ListItem内のテキストコンテンツにはListItemTextコンポーネントを使う。
                        primary属性には表示する内容を渡す */}
                    <ListItemText primary={item.contents}/>
                </ListItem>
            ))}
        </List>
    );
};
export default ItemList;