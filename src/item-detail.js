import React from "react";
import { Card, CardHeader, CardContent, CardActions, Button, Typography} from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { useParams } from 'react-router-dom';

const useStyles = makeStyles({
    border: {
        width: 350
    },
    title:{
        fontSize: 30
    }
});

const ItemDetail = () => {
    const classes = useStyles();
    const { pageno } = useParams();
    return (
        <Card variant='outlined' className={classes.border}>
            <CardHeader title='detail' />
            <CardContent>
                <Typography className={classes.title}>
                    やること: {pageno}
                </Typography>
            </CardContent>
            <CardActions>
                <Button variant='outlined' color='primary'>
                    了解
                </Button>
            </CardActions>
        </Card>
    );
};
export default ItemDetail;