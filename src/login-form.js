import React from "react";
// 利用するコンポーネントをインポート
import {Button, Paper, TextField, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";
// styleオブジェクトを返すフックを作成
const useStyles = makeStyles({
    container: {
        display: 'flex', height: '100vh', backgroundColor: '#eeeeee'
    },
    loginBase: {
        width: '300px', height: '200px', margin: 'auto', padding: '10px'
    },
    field: {width: '80%'},
    loginButton:{ margin: '15px'}
});

const LoginForm = () => {
    // styleオブジェクトをフック的に利用する
    const classes = useStyles();

    // Buttonコンポーネントを利用
    // variantは外見の指定、colorは色の指定　
    // className属性を使って各コンポーネントにスタイルを適用
    return (
        <div className={classes.container} >
            <Paper className={classes.loginBase} >
                {/* Paperは他のコンポーネントを乗せる土台的コンポーネント。
             variantの設定により大きさを指定する */}
                <Typography>ログインしてください。</Typography>
                {/* TextFieldは入力欄とラベルがセットになったコンポーネント。
            labelは画面表記されるラベルの内容 */}
                <TextField className={classes.field} label='Name'/>
                <TextField className={classes.field} label='Password'/>
                {/* Buttonコンポーネントを利用 variantは外見の指定、colorは色の指定 */}

                <Button className={classes.loginButton} variant='contained' color='primary'>
                    ログイン
                </Button>
            </Paper>
        </div>
    );
};
export default LoginForm;