//import logo from './logo.svg';
//import './App.css';
import React from "react"; // node_module配下にあるライブラリは名前だけ
import {Toolbar} from '@material-ui/core';
import {BrowserRouter, Route, Link, Switch} from "react-router-dom";

import LoginForm from "./login-form";
import ItemList from "./item-list";
import Home from './home';// 自作したコンポーネントは相対パスで指定
import ItemDetail from "./item-detail";

const App = () => {
    // BrowserRouterコンポーネントによりルーティングを行う場として設定される
    return (
        <BrowserRouter>
            <div>
                <Toolbar>
                    {/* aタグだめ */}
                    <Link to='/'>トップ</Link>-
                    <Link to='/login'>ログイン</Link>-
                    <Link to='/list'>一覧</Link>

                </Toolbar>
            {/*<LoginForm />*/}
            {/*<ItemList/>*/}
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/login' component={LoginForm} />
                    <Route exact path='/list' component={ItemList} />
                    {/* パラメータの設定例 */}
                    <Route exact path='/detail/:pageno' component={ItemDetail} />
                </Switch>
            </div>
        </BrowserRouter>
    );
};

export default App;
/*JSX
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
      */